#include <iostream>
#include <apt-pkg/cacheset.h>

#include "PackageListerFactory.h"

int main() {
    std::cout << "Listing Packages..." << std::endl;
    auto factory = new PackageListerFactory();
    auto lister = factory->get("debian");
    std::cout << "JSON: " << lister->listPackagesAsJson()  << "\n";

    return 0;
}