//
// University of Passau
// Juliano Efson Sales
//

#ifndef CSALP_DEBIANPACKAGELISTER_H
#define CSALP_DEBIANPACKAGELISTER_H

#include "PackageLister.h"

class DebianPackageLister : public PackageLister {
public:
    list<PackageData*>* listPackages() override;
};


#endif //CSALP_DEBIANPACKAGELISTER_H
