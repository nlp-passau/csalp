//
// University of Passau
// Juliano Efson Sales
//

#include "PackageListerFactory.h"
#include "DebianPackageLister.h"
#include "WindowsPackageLister.h"
#include "RedHatPackageLister.h"

PackageLister *PackageListerFactory::get(string type) {

    if (type == "debian") {
        return new DebianPackageLister();
    } else if (type == "redhat") {
        return new DebianPackageLister();
    } else if (type == "windows") {
        return new DebianPackageLister();
    }

    return nullptr;
}
