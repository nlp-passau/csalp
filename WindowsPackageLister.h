//
// University of Passau
// Juliano Efson Sales
//

#ifndef CSALP_WINDOWSPACKAGELISTER_H
#define CSALP_WINDOWSPACKAGELISTER_H

#include "PackageLister.h"

class WindowsPackageLister : public PackageLister {
public:
    list<PackageData*>* listPackages() override;
};


#endif //CSALP_WINDOWSPACKAGELISTER_H
