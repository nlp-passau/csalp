//
// University of Passau
// Juliano Efson Sales
//

#ifndef CSALP_LPACKAGE_H
#define CSALP_LPACKAGE_H

#include <iostream>
#include <cstring>
#include <list>
#include "Package.h"

using namespace std;

class PackageLister
{
    public:
    string listPackagesAsJson();
    virtual list<PackageData*>* listPackages() = 0;
};

#endif //CSALP_LPACKAGE_H
