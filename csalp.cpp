//
// University of Passau
// Juliano Efson Sales
//

#include "csalp.h"
#include "PackageListerFactory.h"


const char* get_packages(char * platform) {
    auto factory = new PackageListerFactory();
    auto lister = factory->get(platform);

    static string json = lister ? lister->listPackagesAsJson() : "{}";

    delete factory;
    delete lister;
    return json.c_str();
}

