//
// University of Passau
// Juliano Efson Sales
//

#ifndef CSALP_PACKAGE_H
#define CSALP_PACKAGE_H

#include <string>

using namespace std;

struct PackageData {
    std::string name;
    std::string desc;
    std::string arch;
    std::string version;
    std::string state;
    std::string maintainer;
    std::string originalMaintainer;
};

#endif //CSALP_PACKAGE_H
