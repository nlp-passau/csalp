//
// Created by juliano on 06.11.18.
//

#ifndef CSALP_CSALP_H
#define CSALP_CSALP_H

#ifdef __cplusplus
extern "C" {
#endif

    const char* get_packages(char * platform);

#ifdef __cplusplus
}
#endif

#endif //CSALP_CSALP_H
