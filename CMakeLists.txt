cmake_minimum_required(VERSION 3.5)
project(csalp)

if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
    file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/v0.12/conan.cmake"
            "${CMAKE_BINARY_DIR}/conan.cmake")
endif()

include(${CMAKE_BINARY_DIR}/conan.cmake)
conan_cmake_run(REQUIRES jsonformoderncpp/3.2.0@vthiery/stable
        BASIC_SETUP
        BUILD missing)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_BUILD_TYPE "Release")
link_libraries(/usr/lib/x86_64-linux-gnu/libapt-pkg.so)

include_directories("${PROJECT_SOURCE_DIR}")
add_library(csalp SHARED ${HEADER_FILES} PackageLister.h DebianPackageLister.cpp DebianPackageLister.h WindowsPackageLister.cpp WindowsPackageLister.h RedHatPackageLister.cpp RedHatPackageLister.h Package.h PackageLister.cpp PackageListerFactory.cpp PackageListerFactory.h csalp.cpp csalp.h)
install(FILES csalp.h DESTINATION include)
install(TARGETS csalp DESTINATION lib)
