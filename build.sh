#!/usr/bin/env bash
rm -rf cmake-build-release
rm -rf lib
mkdir cmake-build-release
cd cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ../
make
mv lib ../