//
// University of Passau
// Juliano Efson Sales
//

#ifndef CSALP_PACKAGELISTERFACTORY_H
#define CSALP_PACKAGELISTERFACTORY_H

#include "PackageLister.h"

class PackageListerFactory {

public:
    PackageLister* get(string type);
};


#endif //CSALP_PACKAGELISTERFACTORY_H
