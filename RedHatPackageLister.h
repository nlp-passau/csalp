//
// University of Passau
// Juliano Efson Sales
//

#ifndef CSALP_REDHATPACKAGELISTER_H
#define CSALP_REDHATPACKAGELISTER_H

#include "PackageLister.h"

class RedHatPackageLister : public PackageLister {
public:
    list<PackageData*>* listPackages() override;
};


#endif //CSALP_REDHATPACKAGELISTER_H
