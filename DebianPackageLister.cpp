//
// University of Passau
// Juliano Efson Sales
//

#include "DebianPackageLister.h"

#include <iostream>
#include <apt-pkg/cacheset.h>

list<PackageData*>* DebianPackageLister::listPackages() {

    auto packages = new list<PackageData*>();

    pkgInitConfig(*_config);
    pkgInitSystem(*_config, _system);

    pkgCacheFile cache_file;
    pkgCache *cache = cache_file.GetPkgCache();
    pkgRecords Recs(cache_file);

    pkgCache::GrpIterator group = cache->GrpBegin();
    pkgCache::PkgIterator pack = cache->PkgBegin();

    int total = 0;
    for(;!pack.end();pack++)
    {
        if (pack->CurrentState == pkgCache::State::Installed || pack->CurrentState == pkgCache::State::HalfInstalled) {
            PackageData* package = new PackageData();

            pkgCache::DescIterator Desc = pack.VersionList().TranslatedDescription();
            pkgRecords::Parser &P = Recs.Lookup(Desc.FileList());

            package->name = pack.Name();
            package->version = pack.CandVersion() ? pack.CandVersion() : "";
            package->state = "installed";
            package->arch = pack.Arch();
            package->desc = !Desc.end() ? P.LongDesc() : "";
            package->maintainer = !Desc.end() ? P.Maintainer() : "";
            package->originalMaintainer = ""; //TODO

            packages->push_back(package);
            ++total;
        }
    }

    return packages;
}
