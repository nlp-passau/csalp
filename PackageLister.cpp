//
// University of Passau
// Juliano Efson Sales
//

#include <nlohmann/json.hpp>
#include "PackageLister.h"

using json = nlohmann::json;

string PackageLister::listPackagesAsJson() {

    json jsonArray = json::array();
    list<PackageData*>* packages = listPackages();

    for (auto &package : *packages) {
        json jo ={
                {"name", package->name},
                {"desc", package->desc},
                {"state", package->state},
                {"maintainer", package->maintainer},
                {"originalMaintainer", package->originalMaintainer},
                {"version", package->version},
                {"arch", package->arch},
        };

        delete package;
        jsonArray.push_back(jo);
    }
    delete packages;

    auto result = json::object();
    result["packages"] = jsonArray;
    return result.dump();
}
